package com.softtek.academy.javaweb.controller;

 

import java.util.List;

 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

 

import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.services.PersonService;

 

@Controller
public class PersonController {
     	
     	@Autowired
     	private PersonService personService;
     
        @RequestMapping(value = "/person", method = RequestMethod.GET)
        public ModelAndView student() {
            return new ModelAndView("person", "person", new Person());
        }
        
        @RequestMapping(value = "/persondel", method = RequestMethod.GET)
        public ModelAndView studentdel() {
            return new ModelAndView("delete", "persondel", new Person());
        }
        
        @RequestMapping(value = "/personbyid", method = RequestMethod.GET)
        public ModelAndView studentid() {
            return new ModelAndView("personById", "personId", new Person());
        }
        
        @RequestMapping(value = "/personupdate", method = RequestMethod.POST)
        public ModelAndView studentup(@ModelAttribute("id") long id, Model u) {
        	Person one = personService.getById(id);
        	u.addAttribute("one",one);
            return new ModelAndView("update", "personup", new Person());
        }
        /*It displays a form to input data, here "command" is a reserved request attribute
         *which is used to display object data into form
         */
      
        /*It saves object into database. The @ModelAttribute puts request data
         *  into model object. You need to mention RequestMethod.POST method
         *  because default request is GET*/
        
        @RequestMapping(value="/save",method = RequestMethod.POST)
        public String save(@ModelAttribute("emp") Person emp){
        	personService.save(emp);
            return "redirect:/viewperson";//will redirect to viewemp request mapping
        }
        
        @RequestMapping(value="/update",method = RequestMethod.POST)
        public String update(@ModelAttribute("emp") Person emp){
        	personService.updatePerson(emp);
            return "redirect:/viewperson";//will redirect to viewemp request mapping
        }
        
        @RequestMapping(value="/delete",method = RequestMethod.POST)
        public String delete(@ModelAttribute("id") long id){
        	personService.deletePerson(id);
            return "redirect:/viewperson";//will redirect to viewemp request mapping
        }
        
        @RequestMapping(value="/idperson",method = RequestMethod.POST)
        public String Idperson(@ModelAttribute("id") long id, Model o){
        	Person one = personService.getById(id);
        	o.addAttribute("one",one);
            return "showPerson";//will redirect to viewemp request mapping
        }
     
        /* It provides list of employees in model object */
        @RequestMapping("/viewperson")
        public ModelAndView viewemp(Model m){
            List<Person> list=personService.getPersons();
            m.addAttribute("list",list);
            return new ModelAndView("viewperson", "personUP", new Person());
        }
        /* It displays object data into form for the given id.
         * The @PathVariable puts URL data into variable.*/
    
        
}
