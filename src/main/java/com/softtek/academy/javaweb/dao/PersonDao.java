package com.softtek.academy.javaweb.dao;

import java.util.List;

import javax.persistence.Query;

import com.softtek.academy.javaweb.beans.Person;

public interface PersonDao {
	
	int save(Person e);
	
	List<Person> getPersons();

	int deletePerson(long id);

	Person getById(long id);

	int updatePerson(Person s);

}
